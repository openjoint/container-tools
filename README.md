Scripts for building base images:
```
Usage: make <\target>

 * 'print-%' - print-{VAR} - Print variables


 * 'shellcheck' - Bash scripts linter

 * 'install-qemu-user-static' - Register binfmt_misc, qemu-user-static

 ============================
  ** Debian Linux targets **
 ============================

|debian11|
|debian11-java|
|debian11-java-slim|
|debian11-corretto|
|debian11-graal|
|debian11-graal-slim|
|debian11-java-slim-maven|
|debian11-java-slim-gradle|
```