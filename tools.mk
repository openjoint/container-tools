PODMAN? := $(shell podman --version 2>/dev/null)
ifdef DOCKER?
	DOCKER := podman
else
	DOCKER := docker
endif

shellcheck:
	$(PRINT_HEADER)
	shellcheck --severity=error --enable=all --shell=bash $(shell find . -type f -name "*.sh")

bootstrap-qemu-user-static:
	$(PRINT_HEADER)
	$(DOCKER) run --rm --privileged multiarch/qemu-user-static:register --reset
	$(BOOTSTRAP-QEMU) --username=multiarch --project=qemu-user-static --version=latest --arch=x86_64_qemu-aarch64-static --output=/usr/bin